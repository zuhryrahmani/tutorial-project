import React, { Component } from "react";
import { Container } from 'reactstrap';

class Contact extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <Container className='contact'>
        <br/>
        <h1>Contact.</h1>
        <div className="row justify-content-center">
          <div className="col-md-5">
            <div className="card">
              <div className="card-body bg-dark text-light">
                <p className="card-text">You can contact me through information below or send me message directly with filling in the form beside.</p>
              </div>
            </div>
            <ul className="list-group">
              <li className="list-group-item active">Address</li>
              <li className="list-group-item">Jl. Gunung Keraton No. 30, RT.04/RW.05, Kandanggampang, Purbalingga</li>
              <li className="list-group-item active">E-mail</li>
              <li className="list-group-item">zuhryabdirahmani@gmail.com</li>
              <li className="list-group-item active">Mobile Phone</li>
              <li className="list-group-item">+62 821 3579 4634</li>
            </ul>
          </div>
          <div className="col-md-7">
            <form>
              <div className="form-group">
                <label for="exampleFormControlNama">Name</label>
                <input type="text" className="form-control" id="exampleFormControlNama"/>
              </div>
              <div className="form-group">
                <label for="exampleFormControlEmail">E-mail</label>
                <input type="email" className="form-control" id="exampleFormControlEmail"/>
              </div>
              <div className="form-group">
                <label for="exampleFormControlTelp">Phone Number</label>
                <input type="text" className="form-control" id="exampleFormControlTelp"/>
              </div>
              <div className="form-group">
                <label for="exampleFormControlTextarea1">Message</label>
                <textarea className="form-control" id="exampleFormControlTextarea1"></textarea>
              </div>
              <button type="submit" className="btn btn-dark">Send</button>
            </form>
          </div>
        </div>
        <div className="row">
          <div className="col follow">
            <p>or<br/>Follow me on social media</p>
          </div>
        </div>
        <div className="row">
          <div className="col follow-icon">
            <a href="https://web.facebook.com/zuhry.rahmani" target="_blank"><i className="fab fa-facebook-f"></i></a>
            <a href="https://twitter.com/zuhryrahmani" target="_blank"><i className="fab fa-twitter"></i></a>
            <a href="https://www.instagram.com/zuhryabdir/" target="_blank"><i className="fab fa-instagram"></i></a>
            <a href="https://www.linkedin.com/in/zuhryabdirahmani" target="_blank"><i className="fab fa-linkedin-in"></i></a>
          </div>
        </div>
      </Container>
    );
  }
}

export default Contact;
