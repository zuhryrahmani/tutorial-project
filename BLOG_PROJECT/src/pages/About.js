import React, { Component, useState } from "react";
import { Container } from "reactstrap";
import profile from "../assets/images/profile.jpg";
import attaqwa from "../assets/images/portfolio/attaqwa.jpg";
import bayu from "../assets/images/portfolio/bayu.jpg";
import darussalam from "../assets/images/portfolio/darussalam.jpg";
import fickry from "../assets/images/portfolio/fickry.jpg";
import frankfurt from "../assets/images/portfolio/frankfurt.jpg";
import kitchen from "../assets/images/portfolio/kitchen.jpg";
import skatepark from "../assets/images/portfolio/skatepark.jpg";
import skye from "../assets/images/portfolio/skye.jpg";

class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: 'general'
    }
  }
  act(event) {
    if(event.target.textContent === 'General') {
      this.setState({show: 'general'});
    } else if(event.target.textContent === 'Educations') {
      this.setState({show: 'educations'});
    } else if(event.target.textContent === 'Experiences') {
      this.setState({show: 'experiences'});
    } else if(event.target.textContent === 'Skills') {
      this.setState({show: 'skills'});
    } else if(event.target.textContent === 'Organizations') {
      this.setState({show: 'organizations'});
    } else if(event.target.textContent === 'Certificates') {
      this.setState({show: 'certificates'});
    }
    if(!event.target.classList.contains('active')) {
      event.target.classList.add('active');
    } else {
      event.target.classList.remove('active');
    }
  }
  render() {
    const showInfo = () => {
      if(this.state.show === 'general') {
        return (
            <div className="card-body general">
              <h5 className="card-title">Name</h5>
              <p className="card-text">Zuhry Abdi Rahmani</p>
              <h5 className="card-title">Place of Birth</h5>
              <p className="card-text">Purbalingga</p>
              <h5 className="card-title">Date of Birth</h5>
              <p className="card-text">January 13<sup>th</sup> 1995</p>
              <h5 className="card-title">Gender</h5>
              <p className="card-text">Male</p>
              <h5 className="card-title">Nationality</h5>
              <p className="card-text">Indonesian</p>
              <h5 className="card-title">Religion</h5>
              <p className="card-text">Islam</p>
          </div>
        );
      } else if(this.state.show === 'educations') {
        return (
          <div id='educations' className="card-body educations">
            <h4 className="card-title">Front End Developer</h4>
            <p className="card-text">Glints Academy Batch 9<br/><span>November 2020 - February 2020</span></p>
            <h4 className="card-title">Bachelor Degree of Architecture</h4>
            <p className="card-text">Universitas Islam Indonesia<br/><span>August 2013 - January 2019</span></p>
            <h4 className="card-title">Science Major</h4>
            <p className="card-text">SMA Negeri 1 Purbalingga<br/><span>August 2010 - July 2013</span></p>
          </div>
        );
      } else if(this.state.show === 'experiences') {
        return (
          <div className="card-body organizations">
            <h4 className="card-title">Pembinaan Amaliyah Islam</h4>
            <p className="card-text">President / 2010 - 2012</p>
            <h4 className="card-title">Perhimpunan Pegiat Alam Ganesha Muda</h4>
            <p className="card-text">Conservation Dept. Staff / 2011 - 2012</p>
            <h4 className="card-title">Majelis Permusyawaratan Kelas SMA N 1 Purbalingga</h4>
            <p className="card-text">Chief Coordinator / 2011 - 2012</p>
          </div>
        );
      } else if(this.state.show === 'skills') {
        return (
          <div className="card-body experiences">
            <h4 className="card-title">Freelance Architect & Drafter</h4>
            <p className="card-text">Purbalingga - Bekasi<br/><span>April 2019 - September 2020</span></p>
            <p>Responsible for making architectural design, shop drawing, and 3D render of desired building, mostly houses. Already handled 6 projects as a freelancer and many educational-competition purposes projects.</p>
          </div>
        );
      } else if(this.state.show === 'organizations') {
        return (
          <div className="card-body skills">
            <div className="row">
              <div className="col">
                <h4 className="card-title">Coding</h4>
                <p>HTML</p>
                <p>CSS: Bootstrap, Sass</p>
                <p>JavaScript</p>
                <p>Node.js</p>
                <p>Python</p>
                <p>GIT</p>
              </div>
              <div className="col">
                <h4 className="card-title">Design</h4>
                <p>CorelDraw</p>
                <p>GIMP</p>
                <h4 className="card-title">Languages</h4>
                <p>English - Advanced (TOEFL: 550, CEFR: C1)</p>
                <p>Bahasa Indonesia - Native</p>
              </div>
            </div>
          </div>
        );
      } else if(this.state.show === 'certificates') {
        return (
          <div className="card-body certifications">
            <h4 className="card-title">Belajar Dasar Pemrograman Web</h4>
            <p className="card-text">(Dicoding, 4EXGY5KJEXRL)</p>
            <h4 className="card-title">Memulai Pemrograman dengan Python</h4>
            <p className="card-text">(Dicoding, KEXL4LGYWXG2)</p>
            <h4 className="card-title">Belajar Dasar Visualisasi Data</h4>
            <p className="card-text">(Dicoding, GRX5G2L4VX0M)</p>
            <h4 className="card-title">Belajar Membuat Aplikasi Kognitif</h4>
            <p className="card-text">(Dicoding, N9ZOD7890PG5)</p>
            <h4 className="card-title">Dasar-dasar Merancang Aplikasi yang Mudah Digunakan Pengguna/UX Design</h4>
            <p className="card-text">(SkillAcademy, SJA7Y922OFD2J1)</p>
            <h4 className="card-title">Progate Courses (progate.com)</h4>
            <p className="card-text">(HTML&CSS, JavaScript, Sass, GIT, Command Line)</p>
          </div>
        );
      }
    }
    return (
      <Container className='about'>
        <br/>
        <h1>About.</h1>
        <div className="row">
          <div className="col-md-3">
            <div className="card" style={{width: '100%'}}>
              <img src={profile} className="card-img-top" alt="Profile Image"/>
              <div className="card-body">
                <p className="card-text">Former architecture designer and becoming a front-end developer. Switching career because I always love IT in the first place and found my passion at coding around 2019.</p>
              </div>
            </div>
          </div>
          <div className="col-md-9">
            <div className="card text-center" style={{height: '100%'}}>
              <div className="card-header">
                <ul className="nav nav-tabs card-header-tabs">
                  <li className="nav-item">
                    <a id='active1' className="nav-link active" onClick={(event) => {this.act(event)}}>General</a>
                  </li>
                  <li className="nav-item">
                    <a id='active2' className="nav-link" onClick={(event) => {this.act(event)}}>Educations</a>
                  </li>
                  <li className="nav-item">
                    <a id='active3' className="nav-link" onClick={(event) => {this.act(event)}}>Experiences</a>
                  </li>
                  <li className="nav-item">
                    <a id='active4' className="nav-link" onClick={(event) => {this.act(event)}}>Skills</a>
                  </li>
                  <li className="nav-item">
                    <a id='active5' className="nav-link" onClick={(event) => {this.act(event)}}>Organizations</a>
                  </li>
                  <li className="nav-item">
                    <a id='active6' className="nav-link" onClick={(event) => {this.act(event)}}>Certificates</a>
                  </li>
                </ul>
              </div>
              {showInfo()}
            </div>
          </div>
        </div>

        <div className="row">
            <div className="col">
                <h3 id="portfolio">Portfolio</h3>
            </div>
        </div>
        <div className="accordion" id="accordionExample">
            <div className="card">
                <div className="card-header" id="headingOne">
                    <h2 className="mb-0">
                        <button className="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            FRONT-END DEVELOPER <i className="fas fa-chevron-down"></i>
                        </button>
                    </h2>
                </div>
          
                <div id="collapseOne" className="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div className="card-body">
                        <h4>Architecture Portfolio Website</h4>
                        <ul>
                            <li>This project has a purpose as a website that contains all of Zuhry Abdi Rahmani architectural design from college, competition, and freelance's projects.</li>
                            <li>Role: Front-end Developer</li>
                            <li>Sources: <a href="https://gitlab.com/zuhryrahmani/architecture-portfolio-website-project" target="_blank">https://gitlab.com/zuhryrahmani/architecture-portfolio-website-project</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className="card">
                <div className="card-header" id="headingTwo">
                    <h2 className="mb-0">
                        <button className="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            ARCHITECTURE <i className="fas fa-chevron-down"></i>
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" className="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div className="card-body">
                        <div className="row">
                            <div className="col">
                                <img className="hor" src={frankfurt} alt="Frankfurt Islamic Center"/>
                                <p>Frankfurt<br/>Islamic<br/>Center</p>
                            </div>
                            <div className="col">
                                <img className="hor" src={skatepark} alt="Jombor Skatepark"/>
                                <p>Jombor<br/>Skatepark</p>
                            </div>
                            <div className="col">
                                <img className="hor" src={skye} alt="Skye Building"/>
                                <p>Skye<br/>Building</p>
                            </div>
                            <div className="col">
                                <img className="hor" src={darussalam} alt="Masjid Darussalam"/>
                                <p>Darussalam<br/>Mosque</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <img className="ver" src={attaqwa} alt="Masjid Attaqwa"/>
                                <p>Attaqwa<br/>Mosque</p>
                            </div>
                            <div className="col">
                                <img className="hor" src={fickry} alt="Fickry House"/>
                                <p>Fickry<br/>House</p>
                            </div>
                            <div className="col">
                                <img className="ver" src={bayu} alt="Bayu House"/>
                                <p>Bayu<br/>House</p>
                            </div>
                            <div className="col">
                                <img className="ver" src={kitchen} alt="Kitchen Interior"/>
                                <p>Kitchen<br/>Set<br/>Interior</p>
                            </div>
                        </div>
                        <p>You can check my full architecture design portfolio at <a href="https://bit.ly/3ecwIJe" target="_blank">https://bit.ly/3ecwIJe</a></p>
                    </div>
                </div>
            </div>
          </div>
      </Container>
    );
  }
}

// // About Me Pop Out
// let active1 = document.getElementById('active1');
// let active2 = document.getElementById('active2');
// let active3 = document.getElementById('active3');
// let active4 = document.getElementById('active4');
// let active5 = document.getElementById('active5');
// let active6 = document.getElementById('active6');

// let general = document.querySelector('.general');
// let educations = document.querySelector('.educations');
// let experiences = document.querySelector('.experiences');
// let skills = document.querySelector('.skills');
// let organizations = document.querySelector('.organizations');
// let certifications = document.querySelector('.certifications');

// const act1 = () => {
//     if(active1.classList.contains('active') === false) {
//         active1.classList.add('active');
//     }
//     if(active2.classList.contains('active') === true || active3.classList.contains('active') === true || active4.classList.contains('active') === true || active5.classList.contains('active') === true || active6.classList.contains('active') === true) {
//         active2.classList.remove('active');
//         active3.classList.remove('active');
//         active4.classList.remove('active');
//         active5.classList.remove('active');
//         active6.classList.remove('active');
//     }

//     if(general.style.display === 'none') {
//         general.style.display = 'block';
//     }
//     if(educations.style.display === 'block' || experiences.style.display === 'block' || skills.style.display === 'block' || organizations.style.display === 'block' ||certifications.style.display === 'block') {
//         educations.style.display = 'none';
//         experiences.style.display = 'none';
//         skills.style.display = 'none';
//         organizations.style.display = 'none';
//         certifications.style.display = 'none';
//     }
// }
// const act2 = () => {
//     if(active2.classList.contains('active') === false) {
//         active2.classList.add('active');
//     }
//     if(active1.classList.contains('active') === true || active3.classList.contains('active') === true || active4.classList.contains('active') === true || active5.classList.contains('active') === true || active6.classList.contains('active') === true) {
//         active1.classList.remove('active');
//         active3.classList.remove('active');
//         active4.classList.remove('active');
//         active5.classList.remove('active');
//         active6.classList.remove('active');
//     }

//     if(educations.style.display === 'none') {
//         educations.style.display = 'block';
//     }
//     if(general.style.display === 'block' || experiences.style.display === 'block' || skills.style.display === 'block' || organizations.style.display === 'block' ||certifications.style.display === 'block') {
//         general.style.display = 'none';
//         experiences.style.display = 'none';
//         skills.style.display = 'none';
//         organizations.style.display = 'none';
//         certifications.style.display = 'none';
//     }
// }
// const act3 = () => {
//     if(active3.classList.contains('active') === false) {
//         active3.classList.add('active');
//     }
//     if(active1.classList.contains('active') === true || active2.classList.contains('active') === true || active4.classList.contains('active') === true || active5.classList.contains('active') === true || active6.classList.contains('active') === true) {
//         active1.classList.remove('active');
//         active2.classList.remove('active');
//         active4.classList.remove('active');
//         active5.classList.remove('active');
//         active6.classList.remove('active');
//     }

//     if(experiences.style.display === 'none') {
//         experiences.style.display = 'block';
//     }
//     if(educations.style.display === 'block' || general.style.display === 'block' || skills.style.display === 'block' || organizations.style.display === 'block' ||certifications.style.display === 'block') {
//         educations.style.display = 'none';
//         general.style.display = 'none';
//         skills.style.display = 'none';
//         organizations.style.display = 'none';
//         certifications.style.display = 'none';
//     }
// }
// const act4 = () => {
//     if(active4.classList.contains('active') === false) {
//         active4.classList.add('active');
//     }
//     if(active1.classList.contains('active') === true || active2.classList.contains('active') === true || active3.classList.contains('active') === true || active5.classList.contains('active') === true || active6.classList.contains('active') === true) {
//         active1.classList.remove('active');
//         active2.classList.remove('active');
//         active3.classList.remove('active');
//         active5.classList.remove('active');
//         active6.classList.remove('active');
//     }

//     if(skills.style.display === 'none') {
//         skills.style.display = 'block';
//     }
//     if(educations.style.display === 'block' || experiences.style.display === 'block' || general.style.display === 'block' || organizations.style.display === 'block' ||certifications.style.display === 'block') {
//         educations.style.display = 'none';
//         experiences.style.display = 'none';
//         general.style.display = 'none';
//         organizations.style.display = 'none';
//         certifications.style.display = 'none';
//     }
// }
// const act5 = () => {
//     if(active5.classList.contains('active') === false) {
//         active5.classList.add('active');
//     }
//     if(active1.classList.contains('active') === true || active2.classList.contains('active') === true || active3.classList.contains('active') === true || active4.classList.contains('active') === true || active6.classList.contains('active') === true) {
//         active1.classList.remove('active');
//         active2.classList.remove('active');
//         active3.classList.remove('active');
//         active4.classList.remove('active');
//         active6.classList.remove('active');
//     }

//     if(organizations.style.display === 'none') {
//         organizations.style.display = 'block';
//     }
//     if(educations.style.display === 'block' || experiences.style.display === 'block' || skills.style.display === 'block' || general.style.display === 'block' ||certifications.style.display === 'block') {
//         educations.style.display = 'none';
//         experiences.style.display = 'none';
//         skills.style.display = 'none';
//         general.style.display = 'none';
//         certifications.style.display = 'none';
//     }
// }
// const act6 = () => {
//     if(active6.classList.contains('active') === false) {
//         active6.classList.add('active');
//     }
//     if(active1.classList.contains('active') === true || active2.classList.contains('active') === true || active3.classList.contains('active') === true || active4.classList.contains('active') === true || active5.classList.contains('active') === true) {
//         active1.classList.remove('active');
//         active2.classList.remove('active');
//         active3.classList.remove('active');
//         active4.classList.remove('active');
//         active5.classList.remove('active');
//     }

//     if(certifications.style.display === 'none') {
//         certifications.style.display = 'block';
//     }
//     if(educations.style.display === 'block' || experiences.style.display === 'block' || skills.style.display === 'block' || organizations.style.display === 'block' ||general.style.display === 'block') {
//         educations.style.display = 'none';
//         experiences.style.display = 'none';
//         skills.style.display = 'none';
//         organizations.style.display = 'none';
//         general.style.display = 'none';
//     }
// }

export default About;
