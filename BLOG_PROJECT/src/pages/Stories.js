import React from 'react';
import { Container } from 'reactstrap';

// class component
class Stories extends React.Component {

  render() {
    return (
      <Container className='stories'>
        <br/>
        <h1>Stories.</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, reiciendis fugiat, cumque, nam perspiciatis iure exercitationem natus asperiores in est explicabo ex. Laboriosam maxime architecto animi soluta facilis cupiditate a sed eos, dolorem quia iste natus dolores nam recusandae ipsa nihil sint pariatur distinctio quaerat, eligendi consectetur, fugit repellat at. Iste facere accusantium sit omnis porro ipsum voluptatibus, officia maxime non veniam fugiat at similique nisi blanditiis libero eveniet recusandae quas iusto enim dolor aliquam magni illo impedit explicabo? Enim accusamus dolorum adipisci. Blanditiis facilis provident officiis qui iure quas? Consequuntur laudantium magni odio nulla error enim, neque esse suscipit?</p>
      </Container>
    );
  }
}

export default Stories;