import React, { useState } from "react";
import { Switch, Route, Link } from "react-router-dom";

// pages
import Home from "./pages/Home";
import About from "./pages/About";
import Contact from "./pages/Contact";
import Stories from "./pages/Stories";
// pages end

// assets & reactstrap
import "./App.css";
import "./assets/icons/css/all.min.css"
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText,
  Container
} from 'reactstrap';
// assets & reactstrap end

function App() {

  // reactstrap
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  // reactstrap ends

  return (
    <div>
      <Navbar className='navbar' dark expand="md">
        <NavbarToggler onClick={toggle} />
        <Container>
        <Collapse isOpen={isOpen} navbar className='navbarInfoContainer'>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink className='navbarInfo' href="/">Home</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className='navbarInfo' href="/about">About</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className='navbarInfo' href="/stories">Stories</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className='navbarInfo' href="/contact">Contact</NavLink>
            </NavItem>
          </Nav>
          <NavbarBrand className='navbarTitle' href="/" >Zuhry Story.</NavbarBrand>
          <NavbarText>
            <span className='navbarSearch'>
              <input type="text" placeholder="Search Here" className='searchInput'/>
              <i class="fas fa-search" style={{cursor:'pointer'}}></i>
            </span>
            <a href="https://web.facebook.com/zuhry.rahmani" target="_blank"><i className="fab fa-facebook-f navbarIcons"></i></a>
            <a href="https://twitter.com/zuhryrahmani" target="_blank"><i className="fab fa-twitter navbarIcons"></i></a>
            <a href="https://www.instagram.com/zuhryabdir/" target="_blank"><i className="fab fa-instagram navbarIcons"></i></a>
            <a href="https://www.linkedin.com/in/zuhryabdirahmani" target="_blank"><i className="fab fa-linkedin-in navbarIcons"></i></a>
          </NavbarText>
        </Collapse>
        </Container>
      </Navbar>

      <Switch>
        <Route path="/stories">
          <Stories />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
