class Student {
    constructor(name, age, dob, gender, id, hobby) {
        this.name = name;
        this.age = age;
        this.dob = dob;
        this.gender = gender;
        this.id = id;
        if(typeof hobby === 'object') {
            this.hobby = hobby;
        } else {
            this.hobby = [hobby];
        }
    }
}

Student.prototype.setName = function(setName) {
    this.name = setName;
    return this.name;
}
Student.prototype.setAge = function(setAge) {
    this.age = setAge;
    return this.age;
}
Student.prototype.setDateOfBirth = function(setDOB) {
    this.dob = setDOB;
    return this.dob;
}
Student.prototype.setGender = function(setGender) {
    if(setGender.toLowerCase() === 'male') {
        this.gender = 'Male';
        return this.gender;
    } else if(setGender.toLowerCase() === 'female') {
        this.gender = 'Female';
        return this.gender;
    } else {
        return 'Your input is wrong. Choose "Male" or "Female".'
    }
}
Student.prototype.addHobby = function(addHobby) {
    this.hobby.push(addHobby);
    return this.hobby;
}
Student.prototype.removeHobby = function(remove) {
    let index = this.hobby.indexOf(remove);
    if(index >= 0) {
        this.hobby.splice(index, 1);
        return this.hobby;
    } else {
        return 'You cannot remove a hobby that you do not have.';
    }
}
Student.prototype.getData = function() {return this};

let zuhry = new Student('Zuhry', 25, '13 Januari 1995', 'Male', 92020029, 'Gaming');
console.log(zuhry);
console.log(zuhry.name);
console.log(zuhry.hobby);
console.log(zuhry.setName('Nurul'));
console.log(zuhry.setAge(30));
console.log(zuhry.setDateOfBirth('5 Agustus 1995'));
console.log(zuhry.setGender('female'));
console.log(zuhry.addHobby('Swimming'));
console.log(zuhry.removeHobby('Gaming'));
console.log(zuhry.getData());