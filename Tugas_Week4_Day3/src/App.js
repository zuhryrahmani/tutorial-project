
import './App.css';

function App() {
  return (
    <div>
      <div className='ex ex1'>
        <div className="parent blue" style={{backgroundColor:'blue', height:'300px'}}>
          <div className="box coral" style={{backgroundColor:'coral'}}>
            :)
          </div>
        </div>
      </div>
      <div className='ex ex2'>
        <div className="parent white" style={{backgroundColor:'white', width:'400px', margin:'0 auto'}}>
          <div className="box green" style={{backgroundColor:'green', height:'100px'}}>1</div>
          <div className="box green" style={{backgroundColor:'green', height:'100px'}}>2</div>
          <div className="box green" style={{backgroundColor:'green', height:'100px'}}>3</div>
        </div>
      </div>
      <div className='ex ex3'>
        <div className="parent" style={{height:'200px'}}>
          <div className="section yellow" style={{backgroundColor:'yellow'}}>
          Min: 150px / Max: 25%
          </div>
          <div className="section purple" style={{backgroundColor:'purple'}}>
            This element takes the second grid position (1fr), meaning
            it takes up the rest of the remaining space.
          </div>
        </div>
      </div>
      <div className='ex ex4'>
        <div className="parent" style={{height:'200px'}}>
          <header className="blue section" style={{backgroundColor:'blue'}}>Header</header>
          <main className="coral section" style={{backgroundColor:'coral'}}>Main</main>
          <footer className="purple section" style={{backgroundColor:'purple'}}>Footer Content</footer>
        </div>
      </div>
      <div className='ex ex5'>
        <div className="parent" style={{height:'200px'}}>
          <header className="pink section" style={{backgroundColor:'pink'}}>Header</header>
          <div className="left-side blue section" style={{backgroundColor:'blue'}}>Left Sidebar</div>
          <main className="section coral" style={{backgroundColor:'coral'}}> Main Content</main>
          <div className="right-side yellow section" style={{backgroundColor:'yellow'}}>Right Sidebar</div>
          <footer className="green section" style={{backgroundColor:'green'}}>Footer</footer>
        </div>
      </div>
      <div className='ex ex6'>
        <div class="parent white" style={{backgroundColor:'white', height:'200px'}}>
          <div class="span-12 green section" style={{backgroundColor:'green'}}>Span 12</div>
          <div class="span-6 coral section" style={{backgroundColor:'coral'}}>Span 6</div>
          <div class="span-4 blue section" style={{backgroundColor:'blue'}}>Span 4</div>
          <div class="span-2 yellow section" style={{backgroundColor:'yellow'}}>Span 2</div>
        </div>
      </div>
      <div className='ex ex7'>
        <div class="parent white" style={{backgroundColor:'white', height:'200px', width:'600px', margin:'0 auto'}}>
          <div class="box pink" style={{backgroundColor:'pink'}}>1</div>
          <div class="box purple" style={{backgroundColor:'purple'}}>2</div>
          <div class="box blue" style={{backgroundColor:'blue'}}>3</div>
          <div class="box green" style={{backgroundColor:'green'}}>4</div>
        </div>
      </div>
      <div className='ex ex8'>
        <div class="parent white" style={{backgroundColor:'white'}}>
          <div class="card yellow" style={{backgroundColor:'yellow'}}>
            <h3>Title - Card 1</h3>
            <p contenteditable>Medium length description with a few more words here.</p>
            <div class="visual pink" style={{backgroundColor:'pink'}}></div>
          </div>
          <div class="card yellow" style={{backgroundColor:'yellow'}}>
            <h3>Title - Card 2</h3>
            <p contenteditable>Long Description. Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
            <div class="visual blue" style={{backgroundColor:'blue'}}></div>
          </div>
          <div class="card yellow" style={{backgroundColor:'yellow'}}>
            <h3>Title - Card 3</h3>
            <p contenteditable>Short Description.</p>
            <div class="visual green" style={{backgroundColor:'green'}}></div>
          </div>
        </div>
      </div>
      <div className='ex ex9'>
        <div class="parent white" style={{backgroundColor:'white'}}>
          <div class="card purple" style={{backgroundColor:'purple'}}>
            <h1>Title Here</h1>
            <div class="visual yellow" style={{backgroundColor:'yellow'}}></div>
            <p>Descriptive Text. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sed est error repellat veritatis.</p>
          </div>
        </div>
      </div>
      <div className='ex ex10'>
        <div class="parent white" style={{backgroundColor:'white', height:'300px'}}>
          <div class="card blue" style={{backgroundColor:'blue'}}>
            <h1>Video Title</h1>
            <div class="visual green" style={{backgroundColor:'green'}}></div>
            <p>Descriptive Text. This demo works in Chromium 84+.</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
