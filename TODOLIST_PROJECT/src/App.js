
import React, { Component } from 'react';
import TodoItem from './component/content.js';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      database: [
          {name: 'Kasih Makan Singa di Kebun Binatang'},
          {name: 'Minum'},
          {name: 'Tidur'},
          {name: 'Belajar'},
          {name: 'Nonton TV'},
          {name: 'Main Game'},
          {name: 'Boker'},
          {name: 'Bobok'},
          {name: 'Ngemil'},
          {name: 'Ngentut'},
      ]
    }
  }
  render() {
    const data = this.state.database
    return (
      <div>
        <h1 style={{textAlign:'center', fontSize:'80px', margin:'20px 0 0 0'}}>To Do List</h1>
        <div className='container'>
          {data.map(content => {
            return (
              <TodoItem name={content.name} />
            );
          })}
        </div>
      </div>
    );
  }
}

export default App;
