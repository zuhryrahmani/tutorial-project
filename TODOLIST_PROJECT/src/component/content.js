import React, { Component } from 'react';

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            completed: false
        }
    }
    getCheck() {
        if(this.state.completed) {
            this.setState({completed: false});
        } else {
            this.setState({completed: true});
        }
    }
    render() {

        // my style
        let done = {
            float: 'right',
            fontSize: '14px',
            backgroundColor: 'blue',
            color: 'white',
            padding: '5px',
            borderRadius: '3px',
            display: 'none'
        }
        let completed = {
            textDecoration: 'none'
        };
        if(this.state.completed) {
            done.display = 'block';
            completed.textDecoration = 'line-through';
        } else {
            done.display = 'none';
            completed.textDecoration = 'none'
        }
        // my style ends

        return (
            <div className='content'>
                <div className='checkContainer'>
                    <input onClick={() => {this.getCheck()}} type='checkbox'/>
                </div>
                <div className='nameContainer'>
                    <p style={completed}>{this.props.name}</p>
                </div>
                <div className='doneContainer'>
                    <div className='done' style={done}>done</div>
                </div>
            </div>
        );
    }
}

export default Content;