function rangeOfNumbers(startNum, endNum) {
  if(startNum === endNum) {
    return [startNum];
  } else {
    let number = rangeOfNumbers(startNum, endNum-1)
    number.push(endNum);
    return number;
  }
};
