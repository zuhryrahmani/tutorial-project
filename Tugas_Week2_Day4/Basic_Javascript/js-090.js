var myMusic = [
  {
    "artist": "Billy Joel",
    "title": "Piano Man",
    "release_year": 1973,
    "formats": [
      "CD",
      "8T",
      "LP"
    ],
    "gold": true
  }, {
    "artist": "Zuhry",
    "title": "Opo Wae",
    "release_year": 1995,
    "formats": [
      "Blue-ray",
      "DVD",
      "WebRip"
    ],
    "gold": true
  }
  // Add a record here
];
