// contoh alur pengulangan pada algoritma apply job di Jobstreet
// alur pengulangan terjadi ketika user ditanya apakah ingin mengirim lamaran pada JobStreet atau tidak

// javascript
let userApply = true;
let applyJob = false;
while(applyJob === false) {
    if(userApply === true) {
        console.log('User apply to this job.');
        applyJob = true;
    }
}