// contoh alur kondisional pada algoritma apply job di Jobstreet
// alur pengulangan terjadi ketika user ditanya apakah mempunyai akun pada JobStreet atau tidak

// javascript
let haveAcc = false;
let userSignup = false;
let userSignin = true;

if(haveAcc === true) {
    userSignup = false;
    userSignin = true;
    console.log('User menuju Sign In');
} else {
    userSignup = true;
    userSignin = false;
    console.log('User menuju Sign Up');
}